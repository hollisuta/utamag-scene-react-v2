import React from 'react';
import { Link } from 'react-router-dom';
import './magazine.scss';


function Button({path, children}) {
  return (
    <Link className="utamag-btn" to={path}>{children}</Link>
  );
}

function Teaser({children}) {
  return (
  <p className="teaser">{children}</p>
  );
}

export {
  Button,
  Teaser
};
