import React from 'react';
import './VideoBackground.scss';

function VideoBackground({file}) {
    return (
        <div className="fullscreen-bg">
            <video className="fullscreen-bg-video" autoPlay muted loop playsInline>
                <source src={require(`../../content/videos/${file}`)} type="video/mp4" />
            </video>
        </div>
    );
}

export default VideoBackground;
