import React from 'react';

// export const videoState = React.createContext({
//     video: true,
//     audio: true
// });

export const videoState = React.createContext({
    video: false,
    audio: true,
    toggleVideo: () => {},
    toggleAudio: () => {},
});
