import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Teaser } from '../Magazine';
import './overlay.scss';

function Overlay({teaser, title, description}) {

    const [closed, toggleClose] = useState(true);
    const toggle = () => toggleClose(!closed);

    let newDescription;

    if(closed) {
        newDescription = description.substring(0,100);
    } else {
        newDescription = description;
    }

    return (
        <div className={`overlay ${closed ? 'closed' : 'open'}`}>
            <Teaser>{teaser}</Teaser>
            <h2>{title}</h2>
            <p>
                {newDescription}{closed ? '...' : ''}
                <button className="readmore-button" onClick={toggle}>
                    <FontAwesomeIcon icon={closed ? 'chevron-up': 'chevron-down'}/>
                    {closed ? 'Read More' : 'Read Less'}
                </button>
            </p>
        </div>
    );
}

export default Overlay;
