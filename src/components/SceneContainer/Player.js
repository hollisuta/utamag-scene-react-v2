import 'aframe';
import React, { useEffect, useRef, createRef, useState } from 'react';
import { scenes } from '../../content/data';
import './player.scss';

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function Player({data, video, audio, sceneIndex, toggleVideo}) {
  const [initialScene, setInitialScene] = useState(null)
  const prevVideo = usePrevious(sceneIndex);
  const videoRefs = useRef(scenes.map(() => createRef()));

  function askForPermission() {
    if (window.DeviceMotionEvent && typeof DeviceMotionEvent.requestPermission === 'function') {
      DeviceMotionEvent.requestPermission()
      .then(response => {
        if (response === 'granted') {
          window.addEventListener('devicemotion', (e) => {
            // do something with e
          })
        }
      })
      .catch(console.error)
    }

    if (window.DeviceOrientationEvent && typeof DeviceOrientationEvent.requestPermission === 'function') {
      DeviceOrientationEvent.requestPermission()
      .then(response => {
        if (response === 'granted') {
          window.addEventListener('deviceorientation', (e) => {
            // do something with e
          })
        }
      })
      .catch(console.error)
    } else {
      return null
    }
  }

  //On Mount
  useEffect(() => {
    askForPermission();
    setInitialScene(data.id)
  }, []);

  //Scene Change
  useEffect(() => {
    if(prevVideo !== undefined) {
      let oldVideo =  videoRefs.current[prevVideo].current;
      // oldVideo.pause();
      oldVideo.currentTime = 0;
      oldVideo.muted = true;
    }
  }, [prevVideo, sceneIndex]);

  //Video Controls
  useEffect(() => {
      let currentVideo =  videoRefs.current[sceneIndex].current;
      //Video
      if(video && currentVideo.play()) {
        currentVideo.play();
      } else {
        currentVideo.pause();
      }
      //Audio
      if(audio) {
        currentVideo.muted = false;
      } else {
        currentVideo.muted = true;
      }

  }, [video, audio, data.id, sceneIndex]);


  const videoAssets = scenes.map((scene, index) => 
    <video ref={videoRefs.current[index]} key={scene.id} id={scene.id} preload={scene.id === initialScene ? 'auto' : 'none'} autoPlay={scene.id === initialScene ? 'auto' : 'none'} playsInline muted={scene.id === initialScene ? false : true} src={require(`../../content/videos/${scene.videoFile}`)} type="video/mp4" onEnded={() => toggleVideo(false)} ></video>
  );

  return (
    <div className="player">
        <a-scene loading-screen="enabled: false" vr-mode-ui="enabled: false" >
          <a-assets timeout="3000">
              {videoAssets}
          </a-assets> 
          <a-videosphere src={`#${data.id}`} rotation={data.rotation ? data.rotation  : "0 0 0"}></a-videosphere>
        </a-scene>
    </div>
  );
}

export default Player;
