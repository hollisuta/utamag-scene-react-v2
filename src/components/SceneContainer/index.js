import React, { Suspense, lazy, useState, useEffect } from 'react';
import { settings } from '../../content/data';

import { videoState } from './ControlsContext';
const Controls = lazy(() => import ('./Controls'));
const Player = lazy(() => import ('./Player'));
const Overlay = lazy(() => import ('./Overlay'));
// const NextScene = lazy(() => import ('./NextScene'));
const SideMenu = lazy(() => import ('./SideMenu'));

function SceneContainer({data, sceneIndex}) {
  const [localVideoState, setVideoState] = useState(true);
  const [audioState, setAudioState] = useState(true);

  // const toggleVideo = () => setVideoState(!localVideoState)
  const toggleVideo = (value) => setVideoState(value)
  const toggleAudio = () => setAudioState(!audioState)

  if(!data) return null
  
  return (
    <div className="scene-container">
      <Suspense fallback={<div >Loading...</div>}>

        <videoState.Provider value={{video: localVideoState, audio: audioState, toggleVideo: toggleVideo, toggleAudio: toggleAudio}}>
          <videoState.Consumer>
            {({video, audio, toggleVideo, toggleAudio}) => (
              <div>
                <Controls toggleVideo={toggleVideo} toggleAudio={toggleAudio} video={video} audio={audio}/>
                <Player data={data} sceneIndex={sceneIndex} video={video} audio={audio} toggleVideo={toggleVideo}/>
              </div>
            )}
          </videoState.Consumer>
        </videoState.Provider>

        <SideMenu sceneId={data.id}/>
        
        {settings.textOverlay &&
          <Overlay teaser={data.teaser} title={data.title} description={data.description}/>
        }
        
      </Suspense>
    </div>
  );
}

export default SceneContainer;
