import React from 'react';
import { Link } from 'react-router-dom';
import { Teaser } from '../Magazine';
import { scenes } from '../../content/data';
import './nextscene.scss';

function NextScene({sceneIndex}) {

  let nextSceneIndex;

  if(sceneIndex === scenes.length-1) {
    nextSceneIndex = 0;
  } else {
    nextSceneIndex = sceneIndex + 1;
  }

  let nextSceneData = scenes[nextSceneIndex];

  return (
    <div className="next-scene">
        <Link to={`/scenes/${nextSceneData.id}`}>
          <Teaser>Next</Teaser>
          <h3>{nextSceneData.title}</h3>
        </Link>
    </div>
  );
}

export default NextScene;
