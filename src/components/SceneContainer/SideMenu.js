import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Teaser } from '../Magazine';
import { scenes } from '../../content/data';
import './sidemenu.scss';

function SideMenu({sceneId}) {
  const [menuClosed, toggleMenu] = useState(true);
  const toggleSideMenu = () => toggleMenu(!menuClosed);

  useEffect(() => {
    toggleMenu(true)
  }, [sceneId])

  const listItems = scenes.map((scene, index) => 
      <Link className={sceneId === scene.id ? 'active' : ''} to={`/scenes/${scene.id}`} key={index} style={{backgroundImage: `url(${require(`../../content/images/${scene.menuImage}`)})`, backgroundPosition: `${scene.bgPosition ? scene.bgPosition : 'center center'}`}}>
          <img src={require(`../../content/images/${scene.menuImage}`)} alt="scene.title"/>
          <Teaser>{scene.teaser}</Teaser>
          <h3>{scene.title}</h3>
      </Link>
  );

  return (
      <div className={`side-menu ${menuClosed ? 'closed' : 'open'}`}>
      {/* <div className="side-menu open"> */}
        <button className="sidemenu-button" onClick={toggleSideMenu}>
          {menuClosed ? <FontAwesomeIcon icon="th-large"/> : <FontAwesomeIcon icon="times"/>}
        </button>

        <div className={`menu-items`}>
          {listItems}
        </div>
      </div>
  );
}

export default SideMenu;
