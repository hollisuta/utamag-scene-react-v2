import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { videoState } from './ControlsContext';
import './controls.scss';

function Controls({children, toggleVideo, toggleAudio, video, audio}) {

    const toggleVid = () => toggleVideo(!video)
    const toggleAud = () => toggleAudio(!audio)


    // console.log(document.querySelector(`#${}`).readyState)

    return (
        <div>
            <div className="video-controls">
                <button onClick={toggleVid}>{video ? <FontAwesomeIcon icon="pause"/> : <FontAwesomeIcon icon="play"/>}</button>
                <button onClick={toggleAud}>{audio ? <FontAwesomeIcon icon="volume-up"/> : <FontAwesomeIcon icon="volume-mute"/>}</button>
            </div>

            {children}
        </div>
    );
}

export default Controls;
