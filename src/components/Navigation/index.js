import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { splash, social } from '../../content/data';
import './navigation.scss';

function Navigation() {
    const [socialOpen, setSocialOpen] = useState(false);
    const toggleSocial = () => setSocialOpen(!socialOpen)

    const socialLinks = social.map((platform) =>
        <li key={platform.id}>
            <a aria-label={platform.name} href={platform.link} style={{backgroundColor: platform.color}}>
                <FontAwesomeIcon icon={[platform.fontAwesomePrefix, platform.fontAwesomeIcon]}/>
            </a>
        </li>
    )
    return (
        <nav className="navigation">
            <Link className="home-link" to="/"><FontAwesomeIcon icon="home"/>{splash.title}</Link>
            <div className={`share-menu ${socialOpen ? 'open' : 'closed'}`}>
                <button aria-label="Share" className="share-icon" onClick={toggleSocial}>
                    {socialOpen ? <FontAwesomeIcon icon="chevron-down"/> : <FontAwesomeIcon icon="share-alt"/>}
                </button>
                <ul className="share-links">
                    {socialLinks}
                </ul>
            </div>
        </nav>
    );
}

export default Navigation;
