import React from 'react';
import SceneContainer from '../components/SceneContainer';
import Navigation from '../components/Navigation';
import { useParams } from 'react-router-dom';

import { scenes } from '../content/data';
  

function Scene() {
  let slug = useParams();
  let data = scenes.filter((scene) => scene.id === slug.id)
  let sceneIndex = scenes.findIndex((scene) => scene.id === slug.id)

  return (
    <div className="scene">
        <Navigation />
        <SceneContainer data={data[0]} sceneIndex={sceneIndex}/>
    </div>
  );
}

export default Scene;
