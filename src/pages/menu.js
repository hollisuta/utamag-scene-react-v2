import React from 'react';
import { Link } from 'react-router-dom';
import { scenes } from '../content/data';
import { Teaser } from '../components/Magazine';
import Navigation from '../components/Navigation';

import './menu.scss';

function Menu() {
    const listItems = scenes.map((scene, index) =>
        <Link to={`/scenes/${scene.id}`} key={index} style={{backgroundImage: `url(${require(`../content/images/${scene.menuImage}`)})`, backgroundPosition: `${scene.bgPosition ? scene.bgPosition : 'center center'}`}}>
            <div className="column-content">
                <Teaser>{scene.teaser}</Teaser>
                <h2>{scene.title}</h2>
            </div>
        </Link>
    );

  return (
    <div className="menu">
        <Navigation />
        
        <div className="scene-columns">
            {listItems}
        </div>
    </div>
  );
}

export default Menu;
