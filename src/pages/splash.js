import React, { Suspense, lazy } from 'react';
import { splash, scenes } from '../content/data';
import './splash.scss';

import { Button, Teaser } from '../components/Magazine';
const VideoBackground = lazy(() => import ('../components/VideoBackground'));

function Splash() {
  return (
    <div className="splash">
        <Suspense fallback={<div>Loading...</div>}>
            <VideoBackground file={splash.backgroundFile} />
        </Suspense>

        <div className="splash-content story-header">
            {splash.teaser && <Teaser>{splash.teaser}</Teaser>}
            {splash.title && <h1>{splash.title}</h1>}
            {splash.description && <p>{splash.description}</p>}
            {splash.subtext && <p className="small show-for-large">{splash.subtext}</p>}
            <Button path={scenes.length > 1 ? '/menu' : `/scenes/${scenes[0].id}`} >{splash.buttonText}</Button>
        </div>
    </div>
  );
}

export default Splash;
