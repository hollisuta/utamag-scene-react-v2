export const splash  = 
{
  id: "splash",
  title: "Maverick Speaker Series",
  teaser: "Scene 360",
  description: "The Maverick Speakers Series is an entire experience that goes beyond a lecture. Get an in-depth look before, during, and after the event.",
  subtext: "(We recommend using Google Chrome or Apple Safari to view this experience. 360° videos are not yet fully supported by all browsers.)",
  buttonText: "Start the Experience",
  backgroundFile: "intro.mp4"
}

export const settings  = 
{
  textOverlay: true,
  nextPrevButtons: true
}

export const scenes  = [
  {
    id: "master-teachers",
    title: "Master Teachers",
    teaser: "Feat. Terry Cruz",
    description: "Often, our Maverick Speakers conduct master classes for UTA students. Terry Crews was one of the Maverick Speakers Series’ most popular guest lecturers, packing the seats at College Park Center with a dynamic presentation. When actress and playwright Anna Deavere Smith came to campus as part of the 2018 series, she held a master class with theatre arts students and instructors.",
    videoFile: "master-class.mp4",
    thumbnail: "",
    menuImage: "terry-cruz.jpg",
    rotation: "0 80 0",
    bgPosition: '70% center'
  },
  {
    id: "vip-dinner",
    title: "Up Close & Personal",
    teaser: "Feat. Jack Hannah",
    description: "UTA often hosts V.I.P. receptions before lectures, offering guests a unique opportunity to meet the speaker. For Jack Hanna’s reception, 70 guests got a sneak preview and early introductions with the animals prior to their evening lecture appearance.",
    videoFile: "vip-dinner.mp4",
    thumbnail: "",
    menuImage: "jack-hanna.jpg",
    rotation: "0 80 0"
  }
];

// Import the icons in src/App.js
export const social  = [
  {
    id: "facebook",
    name: "Facebook",
    fontAwesomeIcon: "facebook-f",
    fontAwesomePrefix: "fab",
    link: "https://www.facebook.com/dialog/share?display=popup&amp;app_id=126965220683875&amp;redirect_uri=http://www.uta.edu/utamagazine&amp;href=http://www.uta.edu/utamagazine/winter-2020/stories/scene.php",
    color: "#3B5998"
  },
  {
    id: "twitter",
    name: "Twitter",
    fontAwesomeIcon: "twitter",
    fontAwesomePrefix: "fab",
    link: "https://twitter.com/intent/tweet?url=http://www.uta.edu/utamagazine/winter-2020/stories/scene.php&amp;text=I just experienced Maverick Speaker Series 360 on UTA Magazine",
    color: "#00ACED"
  },
  {
    id: "linkedin",
    name: "LinkedIn",
    fontAwesomeIcon: "linkedin-in",
    fontAwesomePrefix: "fab",
    link: "https://www.linkedin.com/shareArticle?mini=true&url=http://www.uta.edu/utamagazine/winter-2020/stories/scene.php&title=Maverick%20Speaker%20Series%20on%20UTA%20Magazine",
    color: "#007FB1"
  },
  {
    id: "email",
    name: "Email",
    fontAwesomeIcon: "envelope",
    fontAwesomePrefix: "fas",
    link: "mailto:?subject=Maverick Speaker Series on UTA Magazine&amp;body=%0A%0Ahttp://www.uta.edu/utamagazine/winter-2020/stories/scene.php",
    color: "#999"
  }
];