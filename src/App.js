import React from 'react';
import { Switch, Route } from "react-router-dom";

import Splash from './pages/splash';
import Menu from './pages/menu';
import Scene from './pages/scene';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faHome,
  faPlay,
  faPause,
  faVolumeUp,
  faVolumeMute,
  faShareAlt,
  faEnvelope,
  faChevronDown,
  faChevronRight,
  faChevronUp,
  faThLarge,
  faTimes
} from '@fortawesome/free-solid-svg-icons';

import {
  faFacebookF,
  faTwitter,
  faLinkedinIn
} from '@fortawesome/free-brands-svg-icons';

library.add(
  faHome,
  faPlay,
  faPause,
  faVolumeUp,
  faVolumeMute,
  faShareAlt,
  faEnvelope,
  faChevronDown,
  faFacebookF,
  faTwitter,
  faLinkedinIn,
  faChevronRight,
  faChevronUp,
  faThLarge,
  faTimes
);

function App() {
  return (
    <div className="app" style={{backgroundColor: '#000'}}>
        <Switch>
          <Route path="/scenes/:id">
            <Scene />
          </Route>
          <Route path="/menu">
            <Menu />
          </Route>
          <Route path="/">
            <Splash />
          </Route>
        </Switch>
    </div>
  );
}

export default App;
